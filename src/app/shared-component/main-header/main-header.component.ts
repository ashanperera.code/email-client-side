import { Component, OnInit, Input } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { deleteUserToken } from '../../helper/token-management';

@Component({
  selector: 'main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {

  @Input() navigationRef!: MatSidenav;

  // TODO : LOAD ALL THE EMAILS LATER.
  sampleEmails: any[] = [
    { name: 'Jane Hew', time: '9:32', message: 'Hey! How is it going?' },
    { name: 'Lloyd Brown', time: '9:18', message: 'Check out my new Dashboard' },
    { name: 'Mark Winstein', time: '9:15', message: 'I want rearrange the appointment' },
    { name: 'Liana Dutti', time: '9:09', message: 'Good news from sale department' }
  ]

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  userSignOut = (signOutProcess: boolean) => {
    if (signOutProcess) {
      deleteUserToken();
      this.router.navigate(['/auth'])
    }
  }

}
