import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'email-notifications',
  templateUrl: './email-notifications.component.html',
  styleUrls: ['./email-notifications.component.scss']
})
export class EmailNotificationsComponent implements OnInit {

  @Input() emails: any[] = [];
  public colors: string[] = [
    'yellow',
    'green',
    'blue',
    'ping'
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
