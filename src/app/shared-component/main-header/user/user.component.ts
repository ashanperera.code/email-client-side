import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  @Input() user: any;
  @Output() signOut: EventEmitter<any> = new EventEmitter<any>();

  public flatlogicEmail: string = "https://flatlogic.com";

  constructor() { }

  ngOnInit(): void {
    if (!this.user) {
      // DEFAULT USER.
      this.user = {
        name: 'Lil',
        lastName: 'Jone',
        email: 'LilJone.22222@gmail.com'
      }
    }
  }

  signOutEmit = () => {
    this.signOut.emit(true);
  }

}
