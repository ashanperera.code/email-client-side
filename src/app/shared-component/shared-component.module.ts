import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MainHeaderComponent } from './main-header/main-header.component';
import { NotificationComponent } from './main-header/notification/notification.component';
import { SearchComponent } from './main-header/search/search.component';
import { UserComponent } from './main-header/user/user.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { EmailNotificationsComponent } from './main-header/email-notifications/email-notifications.component';
import { DateMenuComponent } from './date-menu/date-menu.component';
import { SettingsMenuComponent } from './settings-menu/settings-menu.component';
import { PipesModule } from '../pipes/pipes.module';

// ANGULAR MATERIAL IMPORTS
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [
    MainHeaderComponent,
    NotificationComponent,
    SearchComponent,
    UserComponent,
    SidebarComponent,
    EmailNotificationsComponent,
    DateMenuComponent,
    SettingsMenuComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatBadgeModule,
    PipesModule,
    MatSelectModule,
    RouterModule,
    
  ],
  exports: [
    MainHeaderComponent,
    NotificationComponent,
    SearchComponent,
    UserComponent,
    SidebarComponent,
    EmailNotificationsComponent,
    DateMenuComponent,
    SettingsMenuComponent,
  ]
})
export class SharedComponentModule { }
