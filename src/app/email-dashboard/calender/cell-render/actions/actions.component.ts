import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { ICellRendererParams } from "ag-grid-community";
import { MatDialog } from '@angular/material/dialog';
import { CreateEventComponent } from '../../create-event/create-event.component';
import { DeleteComponent } from '../../delete/delete.component';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements AgRendererComponent {

  eventData: any;
  constructor(public matDialog: MatDialog) { }

  refresh(params: ICellRendererParams): boolean {
    throw new Error('Method not implemented.');
  }

  agInit(params: ICellRendererParams): void {
    this.eventData = params.data;
  }

  openEventPopup = () => {
    this.matDialog.open(CreateEventComponent, {
      height: 'auto',
      width: 'auto',
      data: { mode: 'EDIT', eventData: this.eventData }
    })
  }

  openTaskDeletePopup = () => {
    this.matDialog.open(DeleteComponent, {
      height: 'auto',
      width: 'auto',
    })
  }
}
