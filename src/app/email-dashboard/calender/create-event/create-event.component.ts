import { Component, Input, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {

  @Input() mode: string = 'CREATE';

  constructor(public matDialogRef: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.matDialogRef.disableClose = true;
    if (this.data) {
      this.mode = this.data.mode;
    }
  }

  closeCreateEventModal = () => {
    this.matDialogRef.close();
  }

}
