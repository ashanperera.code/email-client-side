import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { AgGridModule } from 'ag-grid-angular';
import { CalenderComponent } from './calender.component';
import { CalenderRoutingModule } from './calender-routing.module';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CreateEventComponent } from './create-event/create-event.component';
import { ViewEventsComponent } from './view-events/view-events.component';
import { ActionsComponent } from './cell-render/actions/actions.component';
import { DeleteComponent } from './delete/delete.component';
// ANGULAR MATERIAL IMPORTS
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';



@NgModule({
  declarations: [
    CalenderComponent,
    CreateEventComponent,
    ViewEventsComponent,
    ActionsComponent,
    DeleteComponent,
  ],
  imports: [
    CommonModule,
    CalenderRoutingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatChipsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AgGridModule.withComponents([
      ActionsComponent
    ])
  ]
})
export class CalenderModule { }
