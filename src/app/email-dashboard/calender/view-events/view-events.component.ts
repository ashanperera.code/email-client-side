import { Component, OnInit } from '@angular/core';
import { AgGridColumn } from 'ag-grid-angular';
import { MatDialogRef } from '@angular/material/dialog';
import { ActionsComponent } from '../cell-render/actions/actions.component';

@Component({
  selector: 'view-events',
  templateUrl: './view-events.component.html',
  styleUrls: ['./view-events.component.scss']
})
export class ViewEventsComponent implements OnInit {

  gridApi: any;
  gridColumnApi: AgGridColumn[] = []
  defaultColDef: any;
  rowData: any;
  columnDefinitions: any[] = [];

  constructor(private matDialogRef: MatDialogRef<any>) { }

  ngOnInit(): void {
    this.matDialogRef.disableClose = true;
    this.initializeColumn();
  }

  initializeColumn = () => {
    this.columnDefinitions = [
      {
        headerName: "Event Code",
        field: 'eventCode',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: "Event Description",
        field: 'eventDescription',
        filter: 'agTextColumnFilter',
        width: 400
      },
      {
        headerName: "Event Details",
        field: 'eventDetails',
        width: 500
      },
      {
        headerName: "Event Starting Date",
        field: 'eventStartingDate',
        width: 220
      },
      {
        headerName: "Event Ending Date",
        field: 'eventEndingDate',
        width: 220
      },
      {
        headerName: "Actions",
        cellRendererFramework: ActionsComponent
      }
    ]
  }

  onGridReady = (params: any) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();

    this.rowData = [
      {
        eventCode: "E-1",
        eventDescription: "Event sample description",
        eventDetails: "Event details",
        eventStartingDate: "1/2/2021",
        eventEndingDate: "1/4/2021",
      },
      {
        eventCode: "E-2",
        eventDescription: "Event sample description-2",
        eventDetails: "Event details-2",
        eventStartingDate: "1/2/2021",
        eventEndingDate: "1/4/2021",
      }
    ]
  }

  onCloseView = () => {
    this.matDialogRef.close();
  }
}
