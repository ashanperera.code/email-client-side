import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  constructor(private matDialogRef: MatDialogRef<any>) { }

  ngOnInit(): void {
  }

  closeDeleteModal = () => {
    this.matDialogRef.close();
  }
}
