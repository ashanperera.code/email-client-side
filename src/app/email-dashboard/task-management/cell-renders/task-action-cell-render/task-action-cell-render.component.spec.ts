import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskActionCellRenderComponent } from './task-action-cell-render.component';

describe('TaskActionCellRenderComponent', () => {
  let component: TaskActionCellRenderComponent;
  let fixture: ComponentFixture<TaskActionCellRenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskActionCellRenderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskActionCellRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
