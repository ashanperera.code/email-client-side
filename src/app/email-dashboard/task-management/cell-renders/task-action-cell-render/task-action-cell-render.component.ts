import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { ICellRendererParams } from "ag-grid-community";
import { MatDialog } from '@angular/material/dialog';
import { TaskComponent } from '../../task/task.component';
import { DeleteTaskComponent } from '../../delete-task/delete-task.component';

@Component({
  selector: 'task-action-cell-render',
  templateUrl: './task-action-cell-render.component.html',
  styleUrls: ['./task-action-cell-render.component.scss']
})
export class TaskActionCellRenderComponent implements AgRendererComponent {

  taskData: any;

  constructor(public dialog: MatDialog) { }

  refresh(params: ICellRendererParams): boolean {
    throw new Error('Method not implemented.');
  }

  agInit(params: ICellRendererParams): void {
    this.taskData = params.data;
  }

  openTaskPopup = () => {
    this.dialog.open(TaskComponent, {
      height: 'auto',
      width: 'auto',
      data: { mode: 'EDIT', taskData: this.taskData }
    })
  }

  openTaskDeletePopup = () => {
    this.dialog.open(DeleteTaskComponent, {
      height: 'auto',
      width: 'auto',
      data: { taskData: this.taskData }
    })
  }
}
