import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';
import { TaskManagementComponent } from './task-management.component';
import { TaskComponent } from './task/task.component';
import { ViewTasksComponent } from './view-tasks/view-tasks.component';
import { TaskActionCellRenderComponent } from './cell-renders/task-action-cell-render/task-action-cell-render.component';
import { TaskManagementRoutingModule } from './task-management-routing.module';
import { DeleteTaskComponent } from './delete-task/delete-task.component';

// ANGULAR MATERIAL IMPORTS
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [
    TaskManagementComponent,
    TaskComponent,
    ViewTasksComponent,
    TaskActionCellRenderComponent,
    DeleteTaskComponent,
  ],
  imports: [
    CommonModule,
    TaskManagementRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    AgGridModule.withComponents([
      TaskActionCellRenderComponent
    ]),
    HttpClientModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    MatNativeDateModule
  ]
})
export class TaskManagementModule { }
