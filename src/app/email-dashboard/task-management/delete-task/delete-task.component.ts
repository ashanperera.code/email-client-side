import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TaskManagementService } from '../../../services/task-management.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'delete-task',
  templateUrl: './delete-task.component.html',
  styleUrls: ['./delete-task.component.scss']
})
export class DeleteTaskComponent implements OnInit, OnDestroy {
  @BlockUI() blockUI!: NgBlockUI;
  private taskId!: string;
  private taskDeleteSubscription!: Subscription;
  constructor(private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private taskManagementService: TaskManagementService,
    private toastrService: ToastrService) { }

  ngOnInit(): void {
    if (this.data) {
      const { taskData } = this.data;
      this.taskId = taskData.taskId;
    }
  }

  onDeleteHandler = () => {
    if (this.taskId) {
      this.blockUI.start('Processing.......');
      this.taskDeleteSubscription = this.taskManagementService.deleteTask(this.taskId).subscribe(deletedResult => {
        if (deletedResult && deletedResult.validity) {
          this.toastrService.success('Successfully deleted.', 'Success');
          this.taskManagementService.updateTaskGrid.emit(true);
        }
        const deleteTimeout = setTimeout(() => {
          this.blockUI.stop();
          this.closeDeleteModal();
          clearTimeout(deleteTimeout);
        }, 500);
      }, error => {
        this.blockUI.stop();
        console.log(error);
      })
    }
  }

  closeDeleteModal = () => {
    this.matDialogRef.close();
  }

  ngOnDestroy() {
    if (this.taskDeleteSubscription) {
      this.taskDeleteSubscription.unsubscribe();
    }
  }
}
