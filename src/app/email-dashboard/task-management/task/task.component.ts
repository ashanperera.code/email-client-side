import { Component, Inject, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TaskManagementService } from '../../../services/task-management.service';


@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit, OnDestroy {

  @Input() mode: string = 'CREATE';
  @Input() taskData: any = {};

  @Output() afterSave: EventEmitter<any> = new EventEmitter<any>();

  taskManagementSubscriptions: Subscription[] = [];

  taskFormGroup!: FormGroup;

  constructor(
    private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private taskManagementService: TaskManagementService,
    private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.matDialogRef.disableClose = true;
    this.initializeFormGroup();
    if (this.data) {
      this.mode = this.data.mode;
      this.taskData = this.data.taskData;
      this.patchForm();
    }
  }

  initializeFormGroup = () => {
    this.taskFormGroup = new FormGroup({
      taskCode: new FormControl(null, [Validators.required]),
      taskDescription: new FormControl(null),
      taskDetails: new FormControl(null),
      taskStartingDate: new FormControl(null, [Validators.required]),
      taskDueDate: new FormControl(null, [Validators.required]),
      assigneeName: new FormControl(null),
    })
  }

  patchForm = () => {
    this.taskFormGroup.patchValue(this.taskData);
  }

  onCloseCreateTask = () => {
    this.matDialogRef.close();
  }

  updateOrSaveHandler = () => {
    if (this.taskFormGroup.valid) {
      if (this.mode === 'CREATE') {
        // create
        const task = this.taskFormGroup.value;
        this.taskManagementSubscriptions.push(this.taskManagementService.createTask(task).subscribe((createdResult: any) => {
          if (createdResult && createdResult.validity) {
            this.afterSave.emit(createdResult);
            this.toastrService.success('Successfully created', 'Success');
          }
        }, error => {
          console.log(error);
        }));
      } else {
        // update
        const updatedValues = this.taskFormGroup.value;
        this.taskData.taskCode = updatedValues.taskCode;
        this.taskData.taskDescription = updatedValues.taskDescription;
        this.taskData.taskDetails = updatedValues.taskDetails;
        this.taskData.taskStartingDate = updatedValues.taskStartingDate;
        this.taskData.taskDueDate = updatedValues.taskDueDate;
        this.taskData.assigneeName = updatedValues.assigneeName;

        this.taskManagementSubscriptions.push(this.taskManagementService.updateTask(this.taskData).subscribe((updatedTask: any) => {
          if (updatedTask && updatedTask.validity) {
            this.afterSave.emit(this.taskData);
            this.toastrService.success('Successfully updated', 'Success');
          }
        }, error => {
          console.log(error);
        }));
      }
      this.taskManagementService.updateTaskGrid.emit(true);
    }
    else {
      this.toastrService.error('Please check form again', 'Error');
    }
  }

  clearForm = () => { 
    this.taskFormGroup.reset({});
  }

  ngOnDestroy() {
    if (this.taskManagementSubscriptions && this.taskManagementSubscriptions.length > 0) {
      this.taskManagementSubscriptions.forEach(s => {
        s.unsubscribe();
      })
    }
  }
}
