import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ViewTasksComponent } from './view-tasks/view-tasks.component';
import { TaskManagementComponent } from './task-management.component';

const routes: Routes = [
    { path: '', redirectTo: '', pathMatch: 'full' },
    {
        path: '', component: TaskManagementComponent,
        children: [
            { path: '', redirectTo: 'view-tasks', pathMatch: 'full' },
            { path: 'view-tasks', component: ViewTasksComponent },
            { path: '**', redirectTo: 'view-tasks' }
        ]
    },
    {
        path: '**', redirectTo: ''
    }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TaskManagementRoutingModule { }
