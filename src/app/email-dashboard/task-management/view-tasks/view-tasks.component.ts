import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AgGridColumn } from 'ag-grid-angular';
import { Subscription } from 'rxjs';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TaskComponent } from '../task/task.component';
import { TaskActionCellRenderComponent } from '../cell-renders/task-action-cell-render/task-action-cell-render.component';
import { TaskManagementService } from '../../../services/task-management.service';

@Component({
  selector: 'view-tasks',
  templateUrl: './view-tasks.component.html',
  styleUrls: ['./view-tasks.component.scss']
})

export class ViewTasksComponent implements OnInit, OnDestroy {

  @BlockUI() blockUI!: NgBlockUI;
  gridApi: any;
  gridColumnApi: AgGridColumn[] = []
  defaultColDef: any;
  rowData: any;
  taskSubscriptions: Subscription[] = [];
  columnDefinitions: any[] = []

  constructor(public dialog: MatDialog, private taskManagementService: TaskManagementService) { }

  ngOnInit(): void {
    this.defaultColDef = { resizable: false };
    this.initializeColumn();
    this.loadTaskDetails();
    this.listenOnGridUpdate();
  }

  listenOnGridUpdate = () => {
    this.taskSubscriptions.push(this.taskManagementService.updateTaskGrid.subscribe(result => {
      if (result) {
        this.loadTaskDetails();
      }
    }));
  }

  initializeColumn = () => {
    this.columnDefinitions = [
      {
        headerName: "Task Code",
        field: 'taskCode',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: "Task Description",
        field: 'taskDescription',
        filter: 'agTextColumnFilter',
        width: 400
      },
      {
        headerName: "Task Details",
        field: 'taskDetails',
        width: 500
      },
      {
        headerName: "Starting Date",
        field: 'taskStartingDate',
        width: 220
      },
      {
        headerName: "Due Date",
        field: 'taskDueDate',
        width: 220
      },
      {
        headerName: "Assignee Name",
        field: 'assigneeName',
        width: 150
      },
      // TODO: ADD AUDIT LOGS LATER
      {
        headerName: "Actions",
        cellRendererFramework: TaskActionCellRenderComponent
      }
    ]
  }

  loadTaskDetails = () => {
    this.blockUI.start('Loading....');
    this.taskSubscriptions.push(this.taskManagementService.getAllTasks().subscribe((taskServiceResult: any) => {
      if (taskServiceResult && taskServiceResult.validity) {
        this.rowData = taskServiceResult.result;
      }
      const taskGetTImout = setTimeout(() => {
        this.blockUI.stop();
        clearTimeout(taskGetTImout);
      }, 1000);
    }, error => {
      console.log(error);
      this.blockUI.stop();
    }))
  }

  onGridReady = (params: any) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  onOpenTaskPopup = () => {
    this.dialog.open(TaskComponent, {
      height: 'auto',
      width: 'auto'
    })
  }

  ngOnDestroy() {
    if (this.taskSubscriptions && this.taskSubscriptions.length > 0) {
      this.taskSubscriptions.forEach(t => {
        t.unsubscribe();
      })
    }
  }
}