import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateEmailComponent } from '../create-email/create-email.component';
import { ViewEmailComponent } from '../view-email/view-email.component';

@Component({
  selector: 'my-emails',
  templateUrl: './my-emails.component.html',
  styleUrls: ['./my-emails.component.scss']
})
export class MyEmailsComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openEmailCreatePopup = () => {
    this.dialog.open(CreateEmailComponent, {
      height: 'auto',
      width: '70%'
    });
  }

  openEmailView = () => {
    this.dialog.open(ViewEmailComponent, {
      height: 'auto',
      width: '70%'
    })
  }
}
