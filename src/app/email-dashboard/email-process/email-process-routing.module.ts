import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { EmailProcessComponent } from './email-process.component';
import { MyEmailsComponent } from './my-emails/my-emails.component';
import { SentEmailsComponent } from './sent-emails/sent-emails.component';

const routes: Routes = [
    { path: '', redirectTo: '', pathMatch: 'full' },
    {
        path: '', component: EmailProcessComponent,
        children:
            [
                { path: '', redirectTo: 'view-all', pathMatch: 'full' },
                { path: 'view-all', component: MyEmailsComponent },
                { path: 'sent-email', component: SentEmailsComponent }
            ]
    }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmailProcessRoutingModule {

}
