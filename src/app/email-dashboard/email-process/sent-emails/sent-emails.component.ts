import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ViewEmailComponent } from '../view-email/view-email.component';

@Component({
  selector: 'app-sent-emails',
  templateUrl: './sent-emails.component.html',
  styleUrls: ['./sent-emails.component.scss']
})
export class SentEmailsComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openEmailView = () => {
    this.dialog.open(ViewEmailComponent, {
      height: 'auto',
      width: '70%'
    })
  }
}
