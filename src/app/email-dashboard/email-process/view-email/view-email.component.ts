import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'view-email',
  templateUrl: './view-email.component.html',
  styleUrls: ['./view-email.component.scss']
})
export class ViewEmailComponent implements OnInit {
  @BlockUI() blockUI!: NgBlockUI;
  dataModel: any = {};

  constructor(public matDialogRef: MatDialogRef<any>) {
    this.matDialogRef.disableClose = true;
    this.blockUI.start('Loading...');

    const loader = setTimeout(() => {
      this.blockUI.stop();
      clearTimeout(loader);
    }, 2000);
  }

  ngOnInit(): void {
    this.dataModel = `
      <p>Hi Ashan,</p>
      <p>How are you doing ?</p>
      <p>&nbsp;</p>
      <p>thanks&nbsp;</p>
      <p>regards</p>
      <p>Ashan Perera</p>
    `
  }

  onCloseEmailView = () => {
    this.matDialogRef.close();
  }

}
