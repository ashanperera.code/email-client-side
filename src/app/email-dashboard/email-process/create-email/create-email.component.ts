import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { EmailService, LoggedUserService } from '../../../services';

@Component({
  selector: 'create-email',
  templateUrl: './create-email.component.html',
  styleUrls: ['./create-email.component.scss']
})
export class CreateEmailComponent implements OnInit, OnDestroy {

  @BlockUI() blockUI!: NgBlockUI;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  emails: string[] = [];

  emailValue: any = {};
  subject!: string;
  userEmail!: string | null;
  mailSubscription!: Subscription;

  constructor(
    public matDialogRef: MatDialogRef<any>,
    private emailService: EmailService,
    private toastrService: ToastrService,
    private loggedUserService: LoggedUserService) {
    this.blockUI.start('Loading...');

    const loader = setTimeout(() => {
      this.blockUI.stop();
      clearTimeout(loader);
    }, 1000);
  }

  ngOnInit(): void {
    this.matDialogRef.disableClose = true;
    this.userEmail = this.loggedUserService.getUserEmail();
  }

  onEmailCreateClose = () => {
    this.matDialogRef.close();
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.emails.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  remove(email: any): void {
    const index = this.emails.indexOf(email);

    if (index >= 0) {
      this.emails.splice(index, 1);
    }
  }

  deleteAll = () => {
    this.emails = [];
  }

  sendEmail = () => {
    if (this.emails && this.emailValue) {
      this.blockUI.start('Sending...');
      const emailPayload: any = {
        from: this.userEmail,
        to: this.emails,
        subject: this.subject,
        body: this.emailValue,
        attachments: ""
      }

      this.mailSubscription = this.emailService.sendEmail(emailPayload).subscribe(mailResult => {
        if (mailResult) {
          this.toastrService.success('Email successfully sent');
          this.clearForm();
        }
        const loader = setTimeout(() => {
          this.blockUI.stop();
          clearTimeout(loader)
        }, 1000);
      }, ({ error }) => {
        this.toastrService.error(error.error, 'Error');
        this.blockUI.stop();
      })
    } else {
      this.toastrService.warning('Please check required fieldsd', 'Warning');
    }
  }

  clearForm = () => {
    this.emails = [];
    this.subject = "";
    this.emailValue = null;
  }

  ngOnDestroy(): void {
    if (this.mailSubscription) {
      this.mailSubscription.unsubscribe();
    }
  }
}
