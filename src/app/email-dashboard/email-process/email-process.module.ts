import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';
import { EmailProcessComponent } from './email-process.component';
import { EmailProcessRoutingModule } from './email-process-routing.module';
import { MyEmailsComponent } from './my-emails/my-emails.component';
import { CreateEmailComponent } from './create-email/create-email.component';
import { ViewEmailComponent } from './view-email/view-email.component';
// ANGULAR MATERIAL IMPORTS
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { SentEmailsComponent } from './sent-emails/sent-emails.component';

@NgModule({
  declarations: [
    EmailProcessComponent,
    MyEmailsComponent,
    CreateEmailComponent,
    ViewEmailComponent,
    SentEmailsComponent,
  ],
  imports: [
    CommonModule,
    EmailProcessRoutingModule,
    FormsModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatChipsModule,
    EditorModule,
  ]
})
export class EmailProcessModule { }
