import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailProcessComponent } from './email-process.component';

describe('EmailProcessComponent', () => {
  let component: EmailProcessComponent;
  let fixture: ComponentFixture<EmailProcessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailProcessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
