import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { EmailDashboardComponent } from './email-dashboard.component';
import { EmailDashbaordRoutingModule } from './email-dashboard-routing.module';
import { SharedComponentModule } from '../shared-component';
import { HomeComponent } from './home/home.component';
import { ChartModule } from '../chart';

// ANGULAR MATERIAL IMPORTS
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';

@NgModule({
  declarations: [
    EmailDashboardComponent,
    HomeComponent,
  ],
  imports: [
    CommonModule,
    EmailDashbaordRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    SharedComponentModule,
    MatSidenavModule,
    ChartModule,
    HttpClientModule
  ],
  exports: [
  ]
})
export class EmailDashboardModule { }
