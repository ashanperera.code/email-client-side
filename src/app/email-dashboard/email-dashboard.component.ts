import { Component, OnDestroy, ViewChild, OnInit, HostListener } from '@angular/core';
import { MatSidenav, MatDrawerMode } from '@angular/material/sidenav';

@Component({
  selector: 'email-dashboard',
  templateUrl: './email-dashboard.component.html',
  styleUrls: ['./email-dashboard.component.scss']
})
export class EmailDashboardComponent implements OnInit, OnDestroy {

  @ViewChild('sidenav') sidenav!: MatSidenav;
  showSidebar: boolean = true;
  mobileQuery!: MediaQueryList;
  mode: MatDrawerMode = 'side';

  routeChangedRef: any = {};

  constructor() {
    const windowSize = window.innerWidth;
    if (windowSize <= 1200) {
      this.showSidebar = false;
      this.mode = "over";
    }
  }

  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    if (event.target.innerWidth < 1200) {
      this.showSidebar = false;
      this.sideNavOverMode();
    } else {
      this.sideNavSideMode();
    }
  }

  sideNavOverMode = () => {
    this.sidenav.toggle(false);
    this.mode = "over";
  }

  sideNavSideMode = () => {
    this.sidenav.toggle(true);
    this.mode = "side";
  }

  openNavigation = (event: any) => {
    this.showSidebar = event;
  }

  onRouteChange = (event: any) => {
    this.routeChangedRef = event;
  }

  ngOnDestroy(): void {
  }
}
