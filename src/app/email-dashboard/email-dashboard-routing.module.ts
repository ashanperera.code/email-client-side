import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { EmailDashboardComponent } from './email-dashboard.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    { path: '', redirectTo: '', pathMatch: 'full' },
    {
        path: '', component: EmailDashboardComponent, children: [
            { path: '', redirectTo: 'email', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'email', loadChildren: () => import(`./email-process/email-process.module`).then(m => m.EmailProcessModule) },
            { path: 'task', loadChildren: () => import(`./task-management/task-management.module`).then(m => m.TaskManagementModule) },
            { path: 'event-management', loadChildren: () => import(`./calender/calender.module`).then(m => m.CalenderModule) },
            { path: '**', redirectTo: 'email' },
        ]
    },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmailDashbaordRoutingModule { }
