import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  private apiUrl:string = `${environment.base_url}/api/v1/mail`;

  constructor(private httpClient: HttpClient) { }

  sendEmail = (emailPayload: any): Observable<any> => {
    return this.httpClient.post(`${this.apiUrl}/send`, emailPayload)
  }

  replyEmail = (emailPayload: any): Observable<any> => {
    return this.httpClient.post(`${this.apiUrl}/reply`, emailPayload)
  }
}
