import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  authenticateUser = (userLoginPayload: any) => {
    const url = `${environment.base_url}/api/v1/user/sign-in`;
    return this.httpClient.post(url, userLoginPayload);
  }

  registerUser = (registerUser: any) => {
    const url = `${environment.base_url}/api/v1/user/sign-up`;
    return this.httpClient.post(url, registerUser);
  }
}
