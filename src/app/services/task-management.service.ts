import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaskManagementService {

  private baseUrl: string = `${environment.base_url}/api/v1/task`;

  updateTaskGrid: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private httpClient: HttpClient) { }

  createTask = (payload: any): Observable<any> => {
    return this.httpClient.post(`${this.baseUrl}/create`, payload);
  }

  getAllTasks = (): Observable<any> => {
    return this.httpClient.get(`${this.baseUrl}/getAllTasks`);
  }

  getTask = (taskId: string): Observable<any> => {
    return this.httpClient.get(`${this.baseUrl}/getTask/${taskId}`);
  }

  updateTask = (payload: any): Observable<any> => {
    return this.httpClient.put(`${this.baseUrl}/update`, payload);
  }

  deleteTask = (taskId: string, isDelete: boolean = false): Observable<any> => {
    return this.httpClient.delete(`${this.baseUrl}/delete/${taskId}/${isDelete}`);
  }
}
