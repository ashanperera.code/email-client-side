import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { getUserToken } from '../helper/token-management';
import { LoggedUserService } from './logged-user.service';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {

  constructor(private loggedUserService: LoggedUserService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string | null = getUserToken();
    const userId: string = this.loggedUserService.getLoggedUserId();
    if (token) {
      request = request.clone({
        setHeaders: {
          'x-user': `${userId}`,
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(request);
  }
}
