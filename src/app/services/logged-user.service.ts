import { Injectable } from '@angular/core';
import { getDecodedUserToken } from '../helper/token-management';

@Injectable({
  providedIn: 'root'
})
export class LoggedUserService {

  constructor() { }

  storeUser = () => {
    // TODO:
    //use redux later
    const userResult = getDecodedUserToken();
    localStorage.setItem('user', JSON.stringify(userResult));
  }

  getUser = (): any => {
    const stringyFiedResult: any = localStorage.getItem('user');
    const user = JSON.parse(stringyFiedResult);
    return user;
  }

  getUserEmail = (): string | null => {
    const user = this.getUser();
    if (user) {
      return user.userEmail;
    }
    return null;
  }

  getLoggedUserId = () => {
    const user = this.getUser();
    if (user) { 
      return user.userId
    }
  }

}
