import jwt_decode from 'jwt-decode';

export const storeUserToken = (token: any): void => {
    localStorage.setItem('access_token', token);
}

export const getUserToken = (): string | null => {
    return localStorage.getItem('access_token');
}

export const deleteUserToken = () => {
    localStorage.removeItem('access_token');
}

export const getDecodedUserToken = (): any => {
    const token = getUserToken();
    if (token) {
        return jwt_decode(token.toString());
        // localStorage.setItem('user', JSON.stringify(tokenResult));
    }
    return "";
}