import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'visits-chart',
  templateUrl: './visits-chart.component.html',
  styleUrls: ['./visits-chart.component.scss']
})
export class VisitsChartComponent implements OnInit {
  @Input() visitsChartData!: VisitsChartData;
  public colors: typeof colors = colors;

  constructor() { }

  ngOnInit(): void {
  }

}

export enum colors {
  YELLOW = '#ffc260',
  BLUE = '#536DFE',
  LIGHT_BLUE = '#F8F9FF',
  PINK = '#ff4081',
  GREEN = '#3CD4A0',
  VIOLET = '#9013FE'
}

export interface VisitsChartData {
  data: number[];
  registration: string;
  signOut: string;
  rate: string;
  all: string;
}
