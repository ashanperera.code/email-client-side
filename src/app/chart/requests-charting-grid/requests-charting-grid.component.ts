import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'requests-charting-grid',
  templateUrl: './requests-charting-grid.component.html',
  styleUrls: ['./requests-charting-grid.component.scss']
})
export class RequestsChartingGridComponent implements OnInit {
  @Input() supportRequestData: any[] = [];
  public displayedColumns: string[] = ['name', 'email', 'product', 'price', 'date', 'city', 'status'];

  constructor() { }

  ngOnInit(): void {
  }

}
