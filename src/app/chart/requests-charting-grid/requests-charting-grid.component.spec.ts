import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsChartingGridComponent } from './requests-charting-grid.component';

describe('RequestsChartingGridComponent', () => {
  let component: RequestsChartingGridComponent;
  let fixture: ComponentFixture<RequestsChartingGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestsChartingGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsChartingGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
