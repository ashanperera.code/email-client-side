import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'server-chart',
  templateUrl: './server-chart.component.html',
  styleUrls: ['./server-chart.component.scss']
})

export class ServerChartComponent implements OnInit {
  @Input() serverChartData!: ServerChartData;
  charts: Partial<any>[] = [];
  serverDataTitles: string[] = [];
  colors: typeof colors = colors;

  public ngOnInit(): void {
    this.charts = [
      this.initChart(this.serverChartData.firstServerChartData, colors.PINK),
      this.initChart(this.serverChartData.secondServerChartData, colors.BLUE),
      this.initChart(this.serverChartData.thirdServerChartData, colors.YELLOW)
    ];

    this.serverDataTitles = [
      this.serverChartData.firstDataTitle,
      this.serverChartData.secondDataTitle,
      this.serverChartData.thirdDataTitle,
    ]
  }

  public initChart(data: number[], color: string): Partial<any> {
    return {
      chart: {
        type: 'area',
        height: 80,
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        }
      },
      series: [
        {
          name: 'STOCK ABC',
          data: data
        }
      ],
      colors: [color],
      fill: {
        type: 'solid',
        opacity: 0.3
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth',
        width: 2
      },
      labels: this.serverChartData.dates,
      xaxis: {
        type: 'datetime',
        labels: {
          show: false
        },
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        }
      },
      yaxis: {
        max: 50000,
        show: false
      },
      legend: {
        show: false
      },
      grid: {
        show: false,
        padding: {
          bottom: 0,
          left: 0,
          right: 0,
          top: 0
        }
      },
      tooltip: {
        enabled: false
      }
    };
  }
}


// later rmeove to different file
enum colors {
  YELLOW = '#ffc260',
  BLUE = '#536DFE',
  LIGHT_BLUE = '#F8F9FF',
  PINK = '#ff4081',
  GREEN = '#3CD4A0',
  VIOLET = '#9013FE'
}


interface ServerChartData {
  firstServerChartData: number[],
  firstDataTitle: string,
  secondServerChartData: number[],
  secondDataTitle: string,
  thirdServerChartData: number[],
  thirdDataTitle: string,
  dates: string[]
}
