import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskBreakedDownChartComponent } from './task-breaked-down-chart.component';

describe('TaskBreakedDownChartComponent', () => {
  let component: TaskBreakedDownChartComponent;
  let fixture: ComponentFixture<TaskBreakedDownChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskBreakedDownChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskBreakedDownChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
