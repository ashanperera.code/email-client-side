import { Component, OnInit, Input } from '@angular/core';

// move this later to different file.
interface RevenueChartData {
  groupA: number;
  groupB: number;
  groupC: number;
  groupD: number;
}

enum colors {
  YELLOW = '#ffc260',
  BLUE = '#536DFE',
  LIGHT_BLUE = '#F8F9FF',
  PINK = '#ff4081',
  GREEN = '#3CD4A0',
  VIOLET = '#9013FE'
}


@Component({
  selector: 'task-breaked-down-chart',
  templateUrl: './task-breaked-down-chart.component.html',
  styleUrls: ['./task-breaked-down-chart.component.scss']
})
export class TaskBreakedDownChartComponent implements OnInit {

  @Input() taskBreakDownChartData!: RevenueChartData;
  public taskBreakdownChart: any;

  constructor() { }

  ngOnInit(): void {
    this.initChart();
  }

  initChart = () => {
    this.taskBreakdownChart = {
      color: [colors.GREEN, colors.PINK, colors.YELLOW, colors.BLUE],
      tooltip: {
        trigger: 'item'
      },
      legend: {
        top: 'center',
        right: 'right',
        data: ['Group A', 'Group B', 'Group C', 'Group D'],
        textStyle: {
          color: '#6E6E6E'
        }
      },
      series: [{
        type: 'pie',
        radius: ['50%', '70%'],
        center: ['24%', '50%'],
        label: {
          show: false
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        hoverAnimation: false,
        avoidLabelOverlap: false,
        data: [
          {
            name: 'Group A',
            value: this.taskBreakDownChartData.groupA
          },
          {
            name: 'Group B',
            value: this.taskBreakDownChartData.groupB
          },
          {
            name: 'Group C',
            value: this.taskBreakDownChartData.groupC
          },
          {
            name: 'Group D',
            value: this.taskBreakDownChartData.groupD
          },
        ]
      }]
    };
  }
}
