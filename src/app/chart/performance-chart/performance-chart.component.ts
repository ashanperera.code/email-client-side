import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'performance-chart',
  templateUrl: './performance-chart.component.html',
  styleUrls: ['./performance-chart.component.scss']
})
export class PerformanceChartComponent implements OnInit {

  @Input() performanceChartData: any;
  constructor() { }

  ngOnInit(): void {
  }

}
