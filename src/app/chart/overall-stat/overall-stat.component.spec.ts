import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OverallStatComponent } from './overall-stat.component';

describe('OverallStatComponent', () => {
  let component: OverallStatComponent;
  let fixture: ComponentFixture<OverallStatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OverallStatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverallStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
