import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrendModule } from 'ngx-trend';
import { NgApexchartsModule } from 'ng-apexcharts';
import { NgxEchartsModule } from 'ngx-echarts';
import { FormsModule } from '@angular/forms';
import { VisitsChartComponent } from './visits-chart/visits-chart.component';
import { PerformanceChartComponent } from './performance-chart/performance-chart.component';
import { ServerChartComponent } from './server-chart/server-chart.component';
import { SharedComponentModule } from '../shared-component';
import { TaskBreakedDownChartComponent } from './task-breaked-down-chart/task-breaked-down-chart.component';
import { DailyLineChartComponent } from './daily-line-chart/daily-line-chart.component';
import { OverallStatComponent } from './overall-stat/overall-stat.component';
import { RequestsChartingGridComponent } from './requests-charting-grid/requests-charting-grid.component';
// MATERIAL IMPORTS
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';




@NgModule({
  declarations: [
    VisitsChartComponent,
    PerformanceChartComponent,
    ServerChartComponent,
    TaskBreakedDownChartComponent,
    DailyLineChartComponent,
    OverallStatComponent,
    RequestsChartingGridComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatProgressBarModule,
    MatToolbarModule,
    MatGridListModule,
    MatSelectModule,
    MatInputModule,
    TrendModule,
    NgApexchartsModule,
    FormsModule,
    SharedComponentModule,
    NgxEchartsModule
  ],
  exports: [
    VisitsChartComponent,
    PerformanceChartComponent,
    ServerChartComponent,
    TaskBreakedDownChartComponent,
    DailyLineChartComponent,
    OverallStatComponent,
    RequestsChartingGridComponent
  ]
})

export class ChartModule { }
