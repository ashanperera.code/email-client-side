import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoggedUserService } from '../services/logged-user.service';
import { getUserToken } from '../helper/token-management';

@Injectable({
    providedIn: 'root'
})
export class DashboardGuard implements CanActivate {

    constructor(private router: Router, private loggedUserService: LoggedUserService) {
    }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const isLogged: boolean = !!getUserToken();
        if (isLogged) {
            this.loggedUserService.storeUser();
            return true;
        } else {
            this.router.navigate(['/auth']);
            return false;
        }
    }
}
