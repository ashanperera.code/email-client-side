import { Injectable } from '@angular/core';
import { Router, CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { getUserToken } from '../helper/token-management';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanLoad {
    constructor(private router: Router) { }
    canLoad(): Observable<boolean> | Promise<boolean> | boolean {
        const userToken: boolean = !!getUserToken();
        if (userToken) {
            this.router.navigate(['/dashboard']);
            return false;
        } else {

            return true;
        }
    }
}
