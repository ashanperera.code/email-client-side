import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @BlockUI() blockUI!: NgBlockUI;
  registerFormGroup!: FormGroup;
  emailDomain: string = '@cyberwolf.com';

  constructor(private matDialogRef: MatDialogRef<any>, private toastrService: ToastrService, private authService: AuthService) { }

  ngOnInit(): void {
    this.matDialogRef.disableClose = true;
    this.initializeRegisterFormGroup();
  }

  initializeRegisterFormGroup = () => {
    this.registerFormGroup = new FormGroup({
      userName: new FormControl(null, [Validators.required]),
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
      middleName: new FormControl(null),
      userEmail: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      contact: new FormControl(null, [Validators.required]),
      nic: new FormControl(null, [Validators.required]),
      userAddress: new FormControl(null, [Validators.required]),
      passportId: new FormControl(null)
    })
  }

  closeUserDialog = () => {
    this.matDialogRef.close();
  }

  proceedUserRegister = () => {
    if (this.registerFormGroup.valid) {
      this.validateUserEmail();
      this.blockUI.start("Loading.....");
      const userForm: any = this.registerFormGroup.value;
      this.authService.registerUser(userForm).subscribe((registeredUserResult: any) => {
        if (registeredUserResult) {
          this.registerFormGroup.reset({});
          this.closeUserDialog();
          this.toastrService.success('Successfully registered', 'Error');
          this.closeUserDialog();
        }
        this.blockUI.stop();
      }, ({error}) => {
        console.log(error);
        this.toastrService.error(error.error, 'Error');
        this.blockUI.stop();
      })
    } else {
      this.toastrService.error('Please check the form again', 'Error');
    }
  }

  validateUserEmail = () => {
    const userEmail: string = this.registerFormGroup.get('userEmail')?.value;
    if (userEmail) {
      const indexOfAt: number = userEmail.indexOf('@');
      if (indexOfAt > 0) {
        this.registerFormGroup.get('userEmail')?.setValue(`${userEmail.substring(0, indexOfAt)}@cyberwolf.com`);
      } else {
        this.registerFormGroup.get('userEmail')?.setValue(`${userEmail}@cyberwolf.com`);
      }
    }
  }
}
