import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { storeUserToken } from '../../helper/token-management';
import { AuthService } from '../../services/auth.service';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  @BlockUI() blockUI!: NgBlockUI;

  authFormGroup!: FormGroup;
  authSubscription!: Subscription;

  constructor(private authService: AuthService, private router: Router, private toastrService: ToastrService, private matDialog: MatDialog) { }

  ngOnInit(): void {
    this.initiateFormGroup();
  }

  initiateFormGroup = () => {
    this.authFormGroup = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  loginToSystem = () => {
    if (this.authFormGroup.valid) {
      this.blockUI.start("Authenticating ......");
      const userRef = {
        userName: this.authFormGroup.get("email")?.value,
        password: this.authFormGroup.get("password")?.value
      }
      this.authSubscription = this.authService.authenticateUser(userRef).subscribe((signInResponse: any) => {
        if (signInResponse && signInResponse.validity) {
          storeUserToken(signInResponse.result.accessToken);
          this.router.navigate(['/dashboard'])
        }
        this.blockUI.stop();
      }, ({ error }) => {
        console.log(error);
        this.toastrService.error(error.error, 'Error');
        this.blockUI.stop();
      })
    } else {
      this.toastrService.error('Invalid user name or password.');
    }
  }

  openUserRegistration = () => {
    //RegisterComponent
    this.matDialog.open(RegisterComponent, {
      height: 'auto',
      width: '60%'
    })
  }

  ngOnDestroy() {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}
